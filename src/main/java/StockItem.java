public class StockItem {

    private String item; //this is the sku
    private String uuid;
    private String itemDescription;
    private String class_;
    private String size;
    private String container;
    private String delDate;

    public StockItem(String uuid,String item, String itemDescription, String class_, String size, String container) {
        this.item = item;
        this.itemDescription = itemDescription;
        this.class_ = class_;
        this.size = size;
        this.container = container;
        this.uuid = uuid;
    }



    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getClass_() {
        return class_;
    }

    public void setClass_(String class_) {
        this.class_ = class_;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getContainer() {
        return container;
    }

    public void setContainer(String container) {
        this.container = container;
    }

    public String getDelDate() {
        return delDate;
    }

    public void setDelDate(String delDate) {
        this.delDate = delDate;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
