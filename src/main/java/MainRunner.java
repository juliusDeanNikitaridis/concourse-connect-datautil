//import org.springframework.jdbc.core.JdbcTemplate;
//
//
//import org.springframework.jdbc.datasource.DriverManagerDataSource;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.UUID;
//
//public class MainRunner {
//
//    private static JdbcTemplate jdbcTemplate = null;
//
//    public static void main(String[] args) throws ClassNotFoundException {
//
//        Class.forName("org.postgresql.Driver");
//
////        String sql = "SELECT * FROM \"Catalogue\".product_stock";
////        jdbcTemplate = getJdbcTemplate();
////        List<StockItem> stockItems = jdbcTemplate.query(
////                sql,
////                (rs, rowNum) ->
////                        new StockItem(rs.getString("id"),
////                                rs.getString("product_sku"),
////                                rs.getString("description"),
////                                rs.getString("class"),
////                                rs.getString("size"),
////                                rs.getString("container")));
////
////        stockItems.stream().forEach(stockItem -> {
////            UUID productUUID = loadProduct(stockItem);
////            loadAttributes(stockItem, productUUID);
////        });
////        System.out.printf(stockItems.toString());
//        loadStockItemsIntoProjectsTable();
//    }
//
//
//    public static JdbcTemplate getJdbcTemplate() {
//        String driverClassName = "org.postgresql.Driver";
//        String url = "jdbc:postgresql:connectcurrent";
//        String dbUsername = "postgres";
//        String dbPassword = "1234";
//
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName(driverClassName);
//        dataSource.setUrl(url);
//        dataSource.setUsername(dbUsername);
//        dataSource.setPassword(dbPassword);
//
//
//        JdbcTemplate template = new JdbcTemplate();
//        template.setDataSource(dataSource);
//        return template;
//    }
//
//    public static void loadAttributes(StockItem stockItem, UUID productId) {
//        String sql = "insert into \"Catalogue\".attribute(name, value, description,product_id)\n" +
//                "values('class','" + stockItem.getClass_() + "','class attribute','" + productId + "')";
//        jdbcTemplate.execute(sql);
//
//        sql = "insert into \"Catalogue\".attribute(name, value, description,product_id)\n" +
//                "values('container','" + stockItem.getContainer() + "','container attribute','" + productId + "')";
//        jdbcTemplate.execute(sql);
//
//        sql = "insert into \"Catalogue\".attribute(name, value, description,product_id)\n" +
//                "values('size','" + stockItem.getSize() + "','size attribute','" + productId + "')";
//        jdbcTemplate.execute(sql);
//    }
//
//    public static UUID loadProduct(StockItem stockItem) {
//        UUID productId = UUID.randomUUID();
//        String sql = "insert into \"Catalogue\".product(description, sku, id,product_stock_id) " +
//                "values('" + stockItem.getItemDescription() + "'" +
//                ",'" + stockItem.getItem() + "','" + productId + "','" + stockItem.getUuid() + "')";
//        jdbcTemplate.execute(sql);
//        return productId;
//    }
//
//
//
//    public static void loadStockItemsIntoProjectsTable() {
//        jdbcTemplate = getJdbcTemplate();
//        String sql = "select description,id from catalogue_data.product_stock";
//
//        List<StockItemTmp> itemsList = jdbcTemplate.query(
//                sql,
//                (rs, rowNum) -> new StockItemTmp(rs.getString("description"), rs.getString("id"))
//        );
//        itemsList.stream().forEach(item->loadStockAsProduct(item));
//    }
//
//
//
//    //method that will take all the items in product_stock and load it into the projects table
//    public static void loadStockAsProduct(StockItemTmp stockItem) {
//        int categoryId = 11;
//
//        String sql = "INSERT INTO public.projects (group_id, department_id, template_id, title, requestedby, requesteddept, " +
//                "requestdate, approvaldate, approvalby, closedate, " +
//                "owner, entered, enteredby, modified, modifiedby, category_id, portal, allow_guests, news_enabled, details_enabled, " +
//                "team_enabled, plan_enabled, lists_enabled, discussion_enabled, tickets_enabled, documents_enabled, news_label, " +
//                "details_label, team_label, plan_label, lists_label, discussion_label, tickets_label, documents_label," +
//                " est_closedate, budget, budget_currency, portal_default, portal_header, portal_format, portal_key, " +
//                "portal_build_news_body, portal_news_menu, description, allows_user_observers, level, " +
//                "portal_page_type, calendar_enabled, calendar_label, template, wiki_enabled, wiki_label, " +
//                "dashboard_order, news_order, calendar_order, wiki_order, discussion_order, documents_order, " +
//                "lists_order, plan_order, tickets_order, team_order, details_order, dashboard_enabled, " +
//                "dashboard_label, language_id, projecttextid, read_count, read_date, " +
//                "rating_count, rating_value, rating_avg, logo_id, dashboard_description, " +
//                "news_description, calendar_description, wiki_description, discussion_description, " +
//                "documents_description, lists_description, plan_description, tickets_description, team_description, " +
//                "concursive_crm_url, concursive_crm_domain, concursive_crm_code, concursive_crm_client, email1, " +
//                "email2, email3, home_phone, home_phone_ext, home2_phone, home2_phone_ext, home_fax, business_phone, " +
//                "business_phone_ext, business2_phone, business2_phone_ext, business_fax, mobile_phone, pager_number, car_phone, " +
//                "radio_phone, web_page, address_to, addrline1, addrline2, addrline3, city, state, country, postalcode, latitude, " +
//                "longitude, badges_enabled, badges_label, badges_order, badges_description, reviews_enabled, reviews_label, reviews_order, " +
//                "reviews_description, classifieds_enabled, classifieds_label, classifieds_order, classifieds_description, ads_enabled, ads_label, " +
//                "ads_order, ads_description, membership_required, subcategory1_id, subcategory2_id, subcategory3_id, keywords, " +
//                "profile, profile_enabled, profile_label, profile_order, profile_description, source, style, style_enabled, " +
//                "messages_enabled, messages_label, messages_order, messages_description, system_default, shortdescription, " +
//                "instance_id, twitter_id, facebook_page, youtube_channel_id, ustream_id, livestream_id, justintv_id, qik_id, " +
//                "webcasts_enabled, webcasts_label, webcasts_order, webcasts_description, chatroom_enabled, chatroom_label," +
//                " chatroom_order, chatroom_description, photos_enabled, photos_label, photos_order, photos_description, " +
//                "product_id, participants_min, participants_max, participants_count, adoptiondate, discarddate, actual," +
//                " actual_currency, videos_enabled, videos_label, videos_order, videos_description, pricing, parent_id, " +
//                "checkin_latitude, checkin_longitude, checkin_project_id, checkin_location, checkin_date, checkin_user_id, " +
//                "foursquare_venue_id, linked_in_profile, event_profile, apple_ios_url_scheme, apple_ios_store_url, google_android_store_url, " +
//                "rssfeeds_enabled, rssfeeds_label, rssfeeds_order, rssfeeds_description, triggered_action_plan_id, budget_captured, budget_captured_currency, " +
//                "timesheets_enabled, timesheets_label, timesheets_order, timesheets_description, purchase_order_no, mileage_rate, profile_type, pinterest_page, " +
//                "tumblr_page, instagram_page, google_plus_page, flickr_page, related_project_area_gid, participants_rule_id) " +
//                "VALUES " +
//                "(1, null, null, '" + stockItem.description + "', '', '', '2021-01-11 17:40:48.988', '2021-01-11 17:40:59.000', null, null, 2, '2021-01-11 17:40:48.994', 2, " +
//                "'2021-01-11 17:40:59.834', 2," + categoryId + ", false, true, true, false, false, false, true, true, false, true, 'Blog', null, null, null, 'Lists', 'Discussion', null, " +
//                "'Documents', null, 0, null, false, null, null, null, false, false, null, true, 10, null, false, null, false, true, 'Wiki', 2, 3, 8, 4, 5, 6, 7, 11, 13, 14, 15," +
//                " false, null, null, '" + stockItem.description + "', 0, null, 0, 0, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null," +
//                " null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 0, 0, false, null, 16, null, true, " +
//                "'Reviews', 2, null, false, null, 18, null, false, null, 19, null, false, null, null, null, '', false, true, 'Overview', 1, null, null, null, false, false, null, 20, null, false," +
//                "'TestProduct1', null, null, null, null, null, null, null, null, true, 'Webcasts', 8, null, false, null, 22, null, false, null, 6, null, null, null, null, 0, null, null," +
//                " 0, null, false, null, 7, null, null, null, 0, 0, null, null, null, null, null, null, false, null, null, null, false, null, 23, null, null, 0, null, false, null, 3, null, null, " +
//                "0.565, null, null, null, null, null,'" + stockItem.uuid + "', null,null)";
//
//        jdbcTemplate.execute(sql);
//        System.out.println("product has been added " + stockItem.description);
//    }
//
//    static class StockItemTmp {
//        String description;
//        String uuid;
//
//        public StockItemTmp(String description, String uuid) {
//            this.description = description;
//            this.uuid = uuid;
//        }
//    }
//}
