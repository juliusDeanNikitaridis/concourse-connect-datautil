package com.concursive.ukheshe;


import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import javax.crypto.Cipher;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import com.google.gson.JsonObject;


public class UkesheAuthenticationClientNew {
    static Cipher oaepFromAlgo;
    static RestTemplate rest;

    /*****************************************************8
     *
     * I ran all the following commands the generate keys
     *
    --public and private key generated with
    openssl req -nodes -x509 -sha256 -newkey rsa:4096 -keyout "PrivateKey.key" -out "PublicKey.crt" -days 99999

    --Then for the public key to set on Eclipse identity:
     openssl x509 -in PublicKey.crt -pubkey -noout |grep -v "\-\-\-\-\-"| base64 -d| base64 -w0
        --which is set to varible eclipseX509PublicKey

     --Then for the private key to encrypt with:
     cat PrivateKey.key |grep -v "\-\-\-\-\-"| base64 -d| base64 -w0
        --which is set to variable pkcs8EncodedPrivateKey
     *****************************************************/

    //Eclipse public cert
    static byte[] eclipseX509PublicKey = Base64.getDecoder().decode("MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA2cJSqdgRyq2mvH0I0pg+pEBTKvuiQc/Bz2jIv8gvqXEx7l8AnBgaqFi53WnaCpumYF0+xjEqvfjlSWQGv6pkI/ZFCRJtZsehEvJ0DUo4qqByBgRCf28yGCSNxq3Nr/EFEqR+rmwSeegFo1x/QgtHzdOT/32EI0i19vY1eVARM1f2YWfHxZR+zP6g+pm2RKI8cQ01udez4MMB/rhyy3id0/BAk/x/kx3+Y2qyKqqfhTSqjhs/gqnXg9VTvizMzyJoxQZs+jgJJmbw+7WssstdvbqLe7+lJVgpDUdBs3768UPlaoYPDBkWtnS7U7W6zFHSsYu/wk3aYQIMKZqq/vZmBqzV65oajK9Bt2viCRj5RvNClshQW+OLSeIkUrna5Xq1YkhYJTHDHd5qfaOdBDxFXFscfKkn9CM1LZT/siLeNkuBPTpaoRR+Wtp1SheUIc+IC0f3CSYZrwRnuOn2tnGPbkLkEnDPxyx6sU97MEqjtQwgNPZTRI6YD4D4o/NRcmJ76OOtIJuoaMNoWv8p4FIFJ2AxVwQVxRav9LEcByIEZukrMU1wTRnRvhfQgjG3idbg3XiUeTYdgGg1XjYB39JJ16w7cv4QW+J1kpl3BGL+ZgzlIiBZ8vd9Ou/4Ft12PT5TqVd0z4yciiZRL5eDHxn+KiM5GbRzapczI/jEA+vDutcCAwEAAQ==");
    //Tenant Private key
    static byte[] pkcs8EncodedPrivateKey = Base64.getDecoder().decode("MIIJQwIBADANBgkqhkiG9w0BAQEFAASCCS0wggkpAgEAAoICAQDXPJ5nNBTOvqtlJKM6PF80GW2SwRKVkW90tCCli7PHhpPEL9z4nvAzNcKBua2EDZh7eVrHTo4JyJS6Ich615APtgmO64Pzed1pwk3ce/Xu2cXnotk5rrczKX82uuFOI3VkVaypqiG9KSjJT7Bvkscg2oe6YGXABY8Q+NKwsgSICMLu5QRtJohgC5gljcwU3OgTnAM++yhmH/oH4sRQWcJkR3wk4V6qoHOZ8pY6QKnCoq29yoRQxk9DDaJv9z1KLcefxuI6QMveXx1YG5NlPCHR2xJSpgA2ZPAwNyGD64ZluCh0XG39S+mc2SyBWGyB/RKntnd/2VgF5q0RprR0N3y54f2Chif5OclL/0bxGWwtsUg0jzJL8y01/Jm0aC/ShMfswp9rsMdZkBDVjmvp3TTM6XWGPdmwFBxZc8tAlr2ovLncnRsNZ73jIj+6jEYi9+PK7tvlMwYLlkxD9JZ9vbrHFxNeBJx1iRmF7KEh0LF92ff6/LGyHyOq0PqtuGeqwahEwN5GMsCRusib15UBNKwxJh3TTjL0lvtO/ZDfTLw+YW4R0ijTW9HSGn1xsn+G5p2j964vQbBOLsE4IQ9DnptYminjPb8C1096wnq3o9e9HYCWxm4HxxC+JkIOTnmRlwiocrjeXyYxCslo2i4nMrgchNlOTyAsl47ngmdssma5FwIDAQABAoICAEBBY5Sx0qhp+acHSgd/sHhQQwr1wsOOGHUBtWFWlIQzY/fExjYe1Nyk5v1tXiS3Hld2QiiQBg1bqTLsFPq/Uc2u7amSG+Fa90O0cyQoiaIVt2RQlIMI6r5GXCkNn2eBZHFgtlwsY0gien+m36hfbD0doDl+v8GUp0JM1jFT49BstS02h5zCTSJHM9O6QjvUOyBnifyKZGQglbc29p56PXNMd/5WnblYj0pdRoX7efKWOEIZrGik21LMRjsg11ZG1nqXmEAFyhcH1C3Sha1KR1psCUtNLXEPrzjlrIbCkGvbj5A1a8yblhFyMDwpFrOzxpb0/TXYv9N7Kx3C48pyKjSTmd6puYmMlBiFyqEwadDv00bmub4Pwt/jf2FmIpp3g8KzW5YwVEwRwLFlFGlLGhzdMwyEZ6nG/GaFdcjd489IGqWydW+b5XStHiQ3Yvr4gGsqpNnlc87FncJPLi2yvWpILSoEJI0DPQU0awEq9p49RuqbmBnNVVZncVYnT/twd3KXCl06RMcsqiINSFMbF0K2BpwlMeOedD3tWzF2XFKqGMDgzqsWwFMF/Fpecr6iCDnAsiwgIWqV+PwtCOYH1e117Ks8vaWGQgsFCd7CS1gDeR1Sh6+1m7CtGf8wmAW4u/npKF1847H9oo+B0b40CapBuChCUCHZ0p0IeEDLtTURAoIBAQD56CwGzh6JzGvBqbw0HIP7hAYuE/htXZIrLUGZ49W87jJPNdkEAhoQ3zq5NMFgGLuv2akGAi5J1nz5NLkK6XCyOIjdm7K6B73M4gBTaGPkQkL4GsRBZOPXa+6/lKtIbaF0r8eY4Rb2BRzuCxCfmjrZBcH7ew2hmcuP107qqhlyrVrs8LXuwthYCxdG3grqviyYSt8HDQdXmoeJm+xqiwNxM5Wm+RPqjwIyHLXmlVjUiqMTeqGVFsehWDWW2t5yNMG/Ht9cFe+fZawq36TQepVzLUoDgt/pso3FST6rKGuN+9DKGvLXQ9AYEq3tyOO07oEe2F50JnWDsMwz+LGLyAcvAoIBAQDcfAxnnOjAeqFvpcb8ELH/TIazSADjDX7PWk0uT792+L/C+dzW5YJpKZUAB+DvegqPlr0aRK6zdtEwTYNgYRkzqDrwMlMIZmNEtjorX5fbGbeoCYt5gPbXy6Wm45XP7HpPX+HSxADTggA4bjDUqgQOG3QnSx1fi/7HMXRif1cuTzQdolLZ8MBkrDye620awUi/RDmrBbpexRFIDnP4CIYT5D6tR4l2h1bOSELA7BbZLy1rVwqJV71v6PmrEnqmEyvQAtJ0pCOCjbAXSWBCEj3b9By004xI+8NSS5ft22wh95kCBTAyBVRHJDUCwPx/S0IkuB8ZAR5u8X/6Zkm78PKZAoIBAEJCLGohfDdM28UdG6k1ZkjpWDofUV6t0HNiLGKA5cqR3QTjYgaOCBsxFIDGW/7+j8SwEQBm058AH2JzeBxMiHS4SJF5Fon8Q3Zu9Wcn5lQ+vULK1f4Aoegb1dE1ubQaXRRzBpiiFVRlRqZWiLHIUpH31Na4aYTPIgmhLGtLbjPZgqIPsCz9vB6b0jAX7w0OHwTB1SApdCjl78msbaJR+TeqVaNPptgp0os20YOxVTwFBffEK9M7lvJQUOUOYqwlELl7lfz0U/QPIPa9FRWxaq5+84C60mND1BLauKLotJuQiGsNehVZBXNcgIDrInR2SAXFmEVzbPTdi0a8SVamUkECggEBAMCg3BGWA79OJEav1hYY2/oJONHfQW8dNkIgSbgBUmy1zIui0q8pGvOXsaayO3Hd9OYYMySFBwCh+QczYWlNy7/W5Y8+2E5Jbcb1UgLc5IY2hL8Zf7Jq2r4UHKiewx9CImXAhkImnfYr6NdkZeMWmq5ci/FcbdAxNil/aog3asp5xjVxwOLJtLz/wYKyesLaRR1dlOh2+6makIucnPMBN2qfiabIDi/w9AULGs8ENL4QuqTCCeGON2JTb/VnEqKPs0JrhMUFNkT0VvkxURjIaSUGtCHDhReqZa47fwP/JQzUDei28bFx+HyjBHFge/kWnjYIaprk4FrbQQ1aWmcJjlkCggEBAImo85FKN0N4q7tXTNGpDQN4XV0E3l/mOHrpoKUf2jQfBbgxtJfIURPatTsnu3umtAUvHP+sN+fSHV73Q8nwOOyQZQznbC3f4cOREgKes/+a31fejoMqRUomPr29FCrL4O4sqtr+ZgEaK5mfUZ4K4/E9dIIHUKOnFITUbpkot4SFc+P0vh+wEYtPdj3ISPBTaLHz2GjA5vLYGaiOQQq3J48Yn6AmvjI81Kskdt3JGKL0F+GiD3vDMsTxI8E6KUX11h5N6u1dEavpNoy7huM9z8SB5Hc8RJfGLQonwDcFY+V2YRe+WF3G9P7LrLC8+V9xbCwVQx/op0PxUil48xTz2Pk=");

    //Tenant user credentials
    static String identity = "sizwe@connectmobiles24.com";
    static String password = ".80li0bbojy4";

    public static void main(String[] args) throws Exception {
        rest = new RestTemplate(getGenericRequestFactory(5000, 2000));
       // rest.setInterceptors(Collections.singletonList(new RestInterceptor()));
       // rest.setErrorHandler(new RestTemplateResponseErrorHandler());
        Data expectedData = generateChallengeAndExpectedResponse();
        testEclipse(expectedData);
        decryptServerChallenge(expectedData);
    }


    public static Data generateChallengeAndExpectedResponse() throws Exception {
        byte[] randomData = new byte[64];
        SecureRandom.getInstanceStrong().nextBytes(randomData);
        String expectedResponseFromEclipse = Base64.getEncoder().encodeToString(MessageDigest.getInstance("SHA-256").digest(randomData));

        oaepFromAlgo = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding");

        X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(eclipseX509PublicKey);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        oaepFromAlgo.init(Cipher.ENCRYPT_MODE, keyFactory.generatePublic(publicSpec));
        String challengeToEclipse = Base64.getEncoder().encodeToString(oaepFromAlgo.doFinal(randomData));

        System.out.println("challengeToEclipse (What Eclipse should be able to decrypt): " + challengeToEclipse);
        System.out.println("expectedResponseFromEclipse (What Eclipse should return to the login-challenge call): " + expectedResponseFromEclipse);
        return new Data(challengeToEclipse, expectedResponseFromEclipse);
    }

    public static void decryptServerChallenge(Data data) throws Exception {

        PKCS8EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(pkcs8EncodedPrivateKey);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        byte[] serverChallenge = Base64.getDecoder().decode(data.base64EncodedChallenge);
        oaepFromAlgo.init(Cipher.DECRYPT_MODE, keyFactory.generatePrivate(privateSpec));
        byte[] decryptedChallenge = oaepFromAlgo.doFinal(serverChallenge);
        String base64EncodedChallengeResponse = Base64.getEncoder().encodeToString(MessageDigest.getInstance("SHA-256").digest(decryptedChallenge));
        String base64EncodedChallengeHash = Base64.getEncoder().encodeToString(MessageDigest.getInstance("SHA-256").digest(serverChallenge));

        System.out.println("base64EncodedChallengeResponse: " + base64EncodedChallengeResponse);
        System.out.println("base64EncodedChallengeHash: " + base64EncodedChallengeHash);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        JsonObject jo = new JsonObject();
        jo.addProperty("identity", identity);
        jo.addProperty("password", password);
        jo.addProperty("base64EncodedChallengeHash", base64EncodedChallengeHash);
        jo.addProperty("base64EncodedChallengeResponse", base64EncodedChallengeResponse);

        URI uri = new URI("https://eclipse-java-sandbox.jini.rocks/eclipse-conductor/rest/v1/authentication/login");
        RequestEntity<String> reqEntity = new RequestEntity<String>(jo.toString(), headers, HttpMethod.POST, uri);
        ResponseEntity<String> response = rest.exchange(reqEntity, String.class);
        System.out.println(response);

    }

    public static void testEclipse(Data data) throws Exception {

        String getUrl = "https://eclipse-java-sandbox.jini.rocks/eclipse-conductor/rest/v1/authentication/login-challenges" +
                "?clientChallenge=" + URLEncoder.encode(data.challeneToEclipse, "UTF-8") + "&identity="+identity;
        URI uri = new URI(getUrl);
        RequestEntity<String> reqEntity = new RequestEntity<String>(HttpMethod.GET, uri);
        ResponseEntity<String> response = rest.exchange(reqEntity, String.class);
        System.out.println(response);

        System.out.println(URLEncoder.encode(data.challeneToEclipse, "UTF-8"));

        JSONObject jo = new JSONObject(response.getBody());

        data.base64EncodedChallenge = jo.get("base64EncodedChallenge").toString().replaceAll("\"", "");
        data.base64EncodedClientChallengeResponse = jo.get("base64EncodedClientChallengeResponse").toString().replaceAll("\"", "");

        if (data.base64EncodedClientChallengeResponse.equalsIgnoreCase(data.expectedResponse)) {
            System.out.println("Challenge response matched");
            return;
        } else {
            throw new IOException("MisMatched Challenge");
        }
    }

    private static ClientHttpRequestFactory getGenericRequestFactory(int readTimeout, int connectTimeout) {
        SimpleClientHttpRequestFactory scf = new SimpleClientHttpRequestFactory();
        scf.setReadTimeout(readTimeout);
        scf.setConnectTimeout(connectTimeout);

        ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(scf);
        return factory;
    }

    static class Data {
        String challeneToEclipse;
        String expectedResponse;
        String base64EncodedChallenge;
        String base64EncodedClientChallengeResponse;

        Data(String challeneToEclipse, String expectedResponse) {
            this.challeneToEclipse = challeneToEclipse;
            this.expectedResponse = expectedResponse;
        }
    }
}

