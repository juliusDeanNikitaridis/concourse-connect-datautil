//package com.concursive.ukheshe;
//
//import java.io.BufferedInputStream;
//import java.io.DataOutputStream;
//import java.io.InputStream;
//import java.net.URL;
//import java.net.URLEncoder;
//import java.security.KeyFactory;
//import java.security.MessageDigest;
//import java.security.SecureRandom;
//import java.security.spec.PKCS8EncodedKeySpec;
//import java.security.spec.X509EncodedKeySpec;
//import java.util.Base64;
//
//import javax.crypto.Cipher;
//import javax.net.ssl.HttpsURLConnection;
//
//import org.apache.commons.io.IOUtils;
//import org.json.JSONObject;
//
//
//public class UkesheAuthenticationClient {
//
//    //static byte[] x509PublicKey = Base64.getDecoder().decode("MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAtTeC3N1kAYPHHMDPDc+gczmkrNCNzABHbhJOBc4tJ0nieo6iL1GaFbCe4aibA9ArmQu9IM55pNk3CjDFLy3d24ayn3kj5fOdL0tjk23//FyNgaCLAyWDSfH1PUV130XAaf186hJYKinH2Cseist9XlQDvDSuA69UN6ud6jozY79n6Lpz2S4rSNzTFM+3iGqFQkCwanURaiVsZCioIPS5Q++c8FQnlI3q56/Xh+zMp33Y9WaPZvBM/vsoEJljUWvsWlJ8XUUZPSM55YVh1iqFiDkPYWU2PoREllcN3TSTFO5aYJsIBoErVxIVCmoGY0yh5UWUCP9tLvUIWWhDwtoatUCXnktfnCIzSWXvQh3oV9ONANqcwix1hv1LVQdrYGWswPPvtmvsHsZhJXueG7kEO6uA6it81RipS0JEH9mMCbA4OO5GuU9EXfwswXb/pGMoD1/DISJTUQT+a4h+Ex8EyUTegX4NQ3gRQz5cveNtV78aFdfW/wTdTqIJQREQdAZjC2snerWCdF4+Qh+cBpMY0OICM355rJjvY/v9doh2TF6LhpBvAmOanZGYtDow+zI+/xXENQng2hT24w2ok5AG2kYWKqd1DhmUAiic5uKvYyAubTfipyoUozJV5gdbp8yFlWUBd/4BRLRm4hpbH75HDjV1DCpM5/sZau8RXexn4rUCAwEAAQ==");
//    static byte[] eclipseX509PublicKey = Base64.getDecoder().decode("MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA2cJSqdgRyq2mvH0I0pg+pEBTKvuiQc/Bz2jIv8gvqXEx7l8AnBgaqFi53WnaCpumYF0+xjEqvfjlSWQGv6pkI/ZFCRJtZsehEvJ0DUo4qqByBgRCf28yGCSNxq3Nr/EFEqR+rmwSeegFo1x/QgtHzdOT/32EI0i19vY1eVARM1f2YWfHxZR+zP6g+pm2RKI8cQ01udez4MMB/rhyy3id0/BAk/x/kx3+Y2qyKqqfhTSqjhs/gqnXg9VTvizMzyJoxQZs+jgJJmbw+7WssstdvbqLe7+lJVgpDUdBs3768UPlaoYPDBkWtnS7U7W6zFHSsYu/wk3aYQIMKZqq/vZmBqzV65oajK9Bt2viCRj5RvNClshQW+OLSeIkUrna5Xq1YkhYJTHDHd5qfaOdBDxFXFscfKkn9CM1LZT/siLeNkuBPTpaoRR+Wtp1SheUIc+IC0f3CSYZrwRnuOn2tnGPbkLkEnDPxyx6sU97MEqjtQwgNPZTRI6YD4D4o/NRcmJ76OOtIJuoaMNoWv8p4FIFJ2AxVwQVxRav9LEcByIEZukrMU1wTRnRvhfQgjG3idbg3XiUeTYdgGg1XjYB39JJ16w7cv4QW+J1kpl3BGL+ZgzlIiBZ8vd9Ou/4Ft12PT5TqVd0z4yciiZRL5eDHxn+KiM5GbRzapczI/jEA+vDutcCAwEAAQ==");
//
//
//    static byte[] pkcs8EncodedPrivateKey = Base64.getDecoder().decode("MIIJRAIBADANBgkqhkiG9w0BAQEFAASCCS4wggkqAgEAAoICAQC1N4Lc3WQBg8cc" +
//            "wM8Nz6BzOaSs0I3MAEduEk4Fzi0nSeJ6jqIvUZoVsJ7hqJsD0CuZC70gznmk2TcK" +
//            "MMUvLd3bhrKfeSPl850vS2OTbf/8XI2BoIsDJYNJ8fU9RXXfRcBp/XzqElgqKcfY" +
//            "Kx6Ky31eVAO8NK4Dr1Q3q53qOjNjv2founPZLitI3NMUz7eIaoVCQLBqdRFqJWxk" +
//            "KKgg9LlD75zwVCeUjernr9eH7Mynfdj1Zo9m8Ez++ygQmWNRa+xaUnxdRRk9Iznl" +
//            "hWHWKoWIOQ9hZTY+hESWVw3dNJMU7lpgmwgGgStXEhUKagZjTKHlRZQI/20u9QhZ" +
//            "aEPC2hq1QJeeS1+cIjNJZe9CHehX040A2pzCLHWG/UtVB2tgZazA8++2a+wexmEl" +
//            "e54buQQ7q4DqK3zVGKlLQkQf2YwJsDg47ka5T0Rd/CzBdv+kYygPX8MhIlNRBP5r" +
//            "iH4THwTJRN6Bfg1DeBFDPly9421XvxoV19b/BN1OoglBERB0BmMLayd6tYJ0Xj5C" +
//            "H5wGkxjQ4gIzfnmsmO9j+/12iHZMXouGkG8CY5qdkZi0OjD7Mj7/FcQ1CeDaFPbj" +
//            "DaiTkAbaRhYqp3UOGZQCKJzm4q9jIC5tN+KnKhSjMlXmB1unzIWVZQF3/gFEtGbi" +
//            "GlsfvkcONXUMKkzn+xlq7xFd7GfitQIDAQABAoICAQC0J7CoZExkmR5cvm3ZnB02" +
//            "EAtfZqJhObHJgtcgPF11XqWSE7S2+BgIEiOR0W3yd4SwTRRDxiD2up9WOS/+/ElA" +
//            "UgPIOMVweSU9/GPNEKFZwKVJRlKM2UFRRQL6W9CYHB7y//esOHp/yDON+ygM8cn7" +
//            "+egQtgp9TVObWLjQP/YQ5xulKZ+8PIL5l9cdLHeYXdWHDQqVezbJ4NSGKkkjIkgA" +
//            "evj5j6AdtozesljCH69BoAINzV0pCCJzpzqobeVn77P0A2bxzp0Nw7RuMpEWOgWw" +
//            "bJGW0OkBc7s//uGglQDX4THmMpni5Y0xrZ9PUCJzm56jcO1/mHdyJFWdlOMwlmXP" +
//            "ORngU4KSOperr0uzS1NAjY6G/ktquu33MB8jeDe40RohkIh52T0+ttAOr4iW1fka" +
//            "rZsR6Oi9UhIiWreq/xmZfgEPMIjuvoRXj20DExrDVPPw82TvEXEZG5m2cN01V4xk" +
//            "4+ibuYCfmHDBgsKZKQyAstkmdYrjUoNAd9NGGHUufTWRUrE7N4BWoLCLAFQnx11i" +
//            "mezymyXYawoSvT0D+PKPY2/nKFDtMDu0BKhUyaM5FSAcm+loEf74zLPyswxoCdy/" +
//            "PZEDTQ8bdmMwUi5fsex4ECDZMyvXCWkiV8ZBDew3RxGoudMca57N0w0HeMysTThn" +
//            "BQvdc1gOuVmMa13bZQHG7QKCAQEA3vLfzwrXgJb412zIydgMTqWPLeKiI8bOeN0j" +
//            "xlshmj8VQzXvBKit58E4nydogsEYYYhRnkHK51vPZqnP3rkAgQUWaBQI66QS/USE" +
//            "q3qsFQ9cBBvTjIhCSutE63ldey93V3I162rEeMZF3hfrf8s14IxqZeRvLepw13SA" +
//            "PsdxLhcCrohIH2Jt98KvBVYJjXLpKq5TUL7jzcOwapkG/4Ti/mH/L4mPmyH/evCE" +
//            "Hz2X2nq48PunvozIT/w7UFiP7MbjVwGZvKBLO3yPtpWs2wNwG0iauaxBjahQDtqo" +
//            "QWtCXpghV1WhwRJ0HmPGL19ST5WlYzC3iAnY3JCj0R5IlTJXXwKCAQEA0BTez04B" +
//            "xu0+W1B+93k5oFNjnuD/KikcgfAjEn1gB1NVn0upRnlKeyJObNZsDTzSL+zhgmcx" +
//            "BJQeG33fuBqesESulSTthaMXGoDzxSaasFJhwDnibBeAi6jjmCzQbynI2wxOVKZp" +
//            "wnSetR6oXx+5BFVX3jcxp6ODFs4MnvTxH2RXkk5VsuyORLnmqGh6g+CXjRUfw8PM" +
//            "tydDJePY8a6UDQLV5SU4sGW1jGT8HjQ+p4dpWm/OioqZjnRVYRjyKdaJGXGxYFIg" +
//            "6EoNco5SRKL+oq33mbV+jXzPC/NwxhBP+ozmzt8Lmwtj5Iq+UR52L0TRHdqH2a11" +
//            "O+ZweWijc/ViawKCAQEAuBQyhIFAI7UDauMv+a//oWQDDvDDzORWD+cOpbUSzodt" +
//            "Z9v9KIyZYfPga1Afc245UjY6pX9PJ61fVjW5Ivz/FIf90xtPrjf/Ucwj+m3D7XOM" +
//            "Fvxes/bBPZh5hC7l51Lup23uZRz3cXzLrS90y0QejPOC2fQ9c9zRJKPVS4Biy5pG" +
//            "JZLyca9Ueq/s0GwUYQzSxeG/erjR4E/Fto35ADpZLY3+I+LhhkYQ6hHyFNVHLyYK" +
//            "0zYFb0hQD/KDirhExUwF4w24QMzcsRYMlsWIPiDdjDlrPwih63Nf/pO9Pp5v4yia" +
//            "xsA2WPKCm6Dnvq9mh71YG8jakGEJBDwMdhoZ9c3l4wKCAQBrhN08sUopyB0YgheZ" +
//            "OIKWXYB0cD5pQqUAX11ACJi+MEll7mqhh7r8tVVg18sta+gP6477GucxPnfwLLKV" +
//            "JuwomNS9LD+vdf5SK5/Kl5AG3Aq2kTJ2OA6sG7C+ySlKx4SJ62nuQ68SU8BORnEp" +
//            "9fJcUtz8bfiybsMl3YKYAGk3l1nIt8GAmstIp+aPzqy/yCWHOQsY1OvLF/aSdZF8" +
//            "EIFf178+kGUigOu2h9feo5BccJP3rx5CaCvAeExFx+YZyEk/ZIgLBWULonggYXoR" +
//            "DUgWN15hhm7GHV+/BSOYtIUfKf95lbchflk3lpmQwPTAqAaGLo1T5VVWhlRH59HN" +
//            "8VXRAoIBAQDHx9xfGcH2NxXGdmlCPFj4pPvid93c/XfTP+4yfv44GZ6t4DKzdNPl" +
//            "Wp/Pit664Q/Ir5Q52dHUHHFSSYB9BdcDciZ8PhjvUCMilC13OChevPbrNtDo5HjA" +
//            "sjby/IdMCL+qNSInH3FmSTrzLPiOk253EYNmke1Uj0pqPi/gsQdRL6LvQ1x7HzCL" +
//            "jTRuqwSX4p+TcsRxAiO7RMiFnCaXP+BnCf6v6mtla5oFKQhp50T8ACxUjJUWQRNO" +
//            "J2Qvu7POf9gMdHYlKTR+9/CGZeRstf/U6pV48iF6NM3llTgxnaWDVBbC4UNGS6+F" +
//            "bkgTQ0pb1qSFlglr2tl0xfVEP0pfoNyk");
//
//
//    public static void main(String[] args) throws Exception {
//        run();
//    }
//
//
//    public static void run() throws Exception {
//        Data expectedData = generateChallengeAndExpectedResponse(eclipseX509PublicKey);
//        testEclipse(expectedData);
//
//
//        //need to check that the challenge from explise matches the local decrypted challenge
//        if (expectedData.expectedResponse.equals(expectedData.base64EncodedClientChallengeResponse)) {
//            //everything is ok.
//            //decrypt the server challenge ??
//            decryptServerChallenge(pkcs8EncodedPrivateKey, expectedData.base64EncodedChallenge,expectedData);
//            getAuthToken(expectedData);
//        } else {
//            throw new Exception("Decrypted response from eclipse[" + expectedData.base64EncodedClientChallengeResponse + "] " +
//                    "does not match expected response[" + expectedData.expectedResponse + "]");
//        }
//        //complete
//        System.out.printf("COMPLETE ALL [DATA]\n");
//        System.out.printf(expectedData.toString());
//    }
//
//
//    public static Data generateChallengeAndExpectedResponse(byte[] x509PublicKey) throws Exception {
//        byte[] randomData = new byte[64];
//        SecureRandom.getInstanceStrong().nextBytes(randomData);
//        String expectedResponseFromEclipse = Base64.getEncoder().encodeToString(MessageDigest.getInstance("SHA-256").digest(randomData));
//
//        Cipher oaepFromAlgo = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding");
//        X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(x509PublicKey);
//        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//        oaepFromAlgo.init(Cipher.ENCRYPT_MODE, keyFactory.generatePublic(publicSpec));
//        String challengeToEclipse = Base64.getEncoder().encodeToString(oaepFromAlgo.doFinal(randomData));
//        System.out.printf("XXXXXXXXXXXXXXgenerateChallengeAndExpectedResponse completeXXXXXXXXXXXX\n");
//        return new Data(challengeToEclipse, expectedResponseFromEclipse);
//    }
//
//
//
//    public static void decryptServerChallenge(byte[] pkcs8EncodedPrivateKey, String serverChallengeBase64, Data data) throws Exception {
//
//        PKCS8EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(pkcs8EncodedPrivateKey);
//        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//        byte[] serverChallenge = Base64.getDecoder().decode(serverChallengeBase64);
//        Cipher oaepFromAlgo = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding");
//        oaepFromAlgo.init(Cipher.DECRYPT_MODE, keyFactory.generatePrivate(privateSpec));
//        byte[] decryptedChallenge = oaepFromAlgo.doFinal(serverChallenge);
//        String base64EncodedChallengeResponse = Base64.getEncoder().encodeToString(MessageDigest.getInstance("SHA-256").digest(decryptedChallenge));
//        String base64EncodedChallengeHash = Base64.getEncoder().encodeToString(MessageDigest.getInstance("SHA-256").digest(serverChallenge));
//
//        data.base64EncodedChallengeResponse = base64EncodedChallengeResponse;
//        data.base64EncodedChallengeHash = base64EncodedChallengeHash;
//        System.out.printf("XXXXXXXXdescryptServerChallennge complete\n");
//    }
//
//    public static void testEclipse(Data data) throws Exception {
//
//        String getUrl = "https://eclipse-java-sandbox.jini.rocks/eclipse-conductor/rest/v1/authentication/login-challenges" +
//                "?clientChallenge=" + URLEncoder.encode(data.challeneToEclipse, "UTF-8") + "&identity=julius@connectmobiles24.com";
////                "\" -H  \"accept: application/json";
//
//        System.out.println(data.challeneToEclipse);
//
//
//        URL checkTransactionURL = new URL(getUrl);
//        InputStream is = null;
//
//        HttpsURLConnection conn = (HttpsURLConnection) checkTransactionURL.openConnection();
//        conn.setRequestMethod("GET");
//        conn.setRequestProperty("accept", "application/json"); //?????????????????????????
//        int responseCode = conn.getResponseCode();
//
//        if (responseCode >= 400) is = conn.getErrorStream();
//        else is = new BufferedInputStream(conn.getInputStream());
//
//        String webserviceResponse = IOUtils.toString(is);
//
//        JSONObject jsonObject = new JSONObject(webserviceResponse);
//        data.base64EncodedChallenge = jsonObject.get("base64EncodedChallenge").toString();
//        data.base64EncodedClientChallengeResponse = jsonObject.get("base64EncodedClientChallengeResponse").toString();
//        System.out.printf("XXXXXXXXXXXXXXXtestEclipse completeXXXXXXXXXXXXXXX\n");
//        System.out.printf(webserviceResponse+"\n");
//    }
//
//
//    public static void getAuthToken(Data data) throws Exception {
//
//        InputStream is = null;
//        String responseString = "NOT SET";
//        String urlString = "https://eclipse-java-sandbox.jini.rocks/eclipse-conductor/rest/v1/authentication/login";
//
//
//        URL url = new URL(urlString);
//        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
//        conn.setRequestMethod("POST");
//
//        conn.setRequestProperty("Content-Type", "application/json");
//        conn.setRequestProperty("accept", "application/json");
//
//        conn.setDoInput(true);
//        conn.setDoOutput(true);
//
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("base64EncodedChallengeHash",data.base64EncodedChallengeHash);
//        jsonObject.put("base64EncodedChallengeResponse",data.base64EncodedChallengeResponse);
//        jsonObject.put("identity","julius@connectmobiles24.com");
//        jsonObject.put("password",".53q69p4mzar");
//        System.out.printf("JSONNNNNNNNNNNNNNNNNNNNNNNN\n");
//        System.out.printf(jsonObject.toString());
//
////        String jsonRequestData = "{\\\"base64EncodedChallengeHash\\\":\\\"" + data.base64EncodedChallengeHash + "\\\"," +
////                "\\\"base64EncodedChallengeResponse\\\":\\\"" + data.base64EncodedChallengeResponse + "\\\"," +
////                "\\\"identity\\\":\\\"julius@connectmobiles24.com\\\",\\\"password\\\":\\\"5NKnTEjvhnefL6L\\\"}";
//
//        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
//        wr.writeBytes(jsonObject.toString());
//        wr.flush();
//        wr.close();
//        int responseCode = conn.getResponseCode();
//
//        if (responseCode >= 400) {
//            is = conn.getErrorStream();
//        } else {
//            is = conn.getInputStream();
//        }
//        responseString = IOUtils.toString(is);
//        System.out.printf("XXXXXXXXXXXXXXget Authtoken completeXXXXXXXXXXXXXXXXXX\n");
//        System.out.printf(responseString+"\n");
//
////            JSONObject jsonObject = new JSONObject(responseString);
////            String transactionID = jsonObject.get("id").toString();
//
//    }
//
//
//    static class Data {
//        String challeneToEclipse = "NOT SET";
//        String expectedResponse = "NOT SET";
//        String base64EncodedChallenge = "NOT SET";
//        String base64EncodedClientChallengeResponse = "NOT SET";
//        String base64EncodedChallengeResponse = "NOT SET";
//        String base64EncodedChallengeHash = "NOT SET";
//
//        Data(String challeneToEclipse, String expectedResponse) {
//            this.challeneToEclipse = challeneToEclipse;
//            this.expectedResponse = expectedResponse;
//        }
//
//
//        @Override
//        public String toString() {
//            return "Data{" +
//                    "challeneToEclipse='" + challeneToEclipse + '\'' +"\n"+
//                    ", expectedResponse='" + expectedResponse + '\'' +"\n"+
//                    ", base64EncodedChallenge='" + base64EncodedChallenge + '\'' +"\n"+
//                    ", base64EncodedClientChallengeResponse='" + base64EncodedClientChallengeResponse + '\'' +"\n"+
//                    ", base64EncodedChallengeResponse='" + base64EncodedChallengeResponse + '\'' +"\n"+
//                    ", base64EncodedChallengeHash='" + base64EncodedChallengeHash + '\'' +"\n"+
//                    '}';
//        }
//    }
//}
//
