package com.concursive.peachpayments;

public class ActionPlanFile {

//    public static String fileString = "[{form name=\"Soil Management\"}]\n" +
//            "---\n" +
//            "[{group value=\"History of Block\" display=\"false\"}]\n" +
//            "[{description value=\"what was previously planted/ for how long in continuous production etc.\"}]\n" +
//            "---\n"
    public static final String fileString = ""+
            "[{label value=\"Compliant\"}]\n" +
            "[{field type=\"checkbox\" name=\"compliant\"}]\n" +
            "[{entry value=\"true\"}]\n" +
            "---\n" +
            "[{label value=\"Evidence/Comment sec supplier\"}]\n" +
            "[{field type=\"textarea\" name=\"evidence/comment_sec_supplier\"}]\n" +
            "[{entry value=\"6 year rotation\"}]\n" +
            "---\n" +
            "[{label value=\"Timeline\"}]\n" +
            "[{field type=\"datetime\" name=\"timeline\"}]\n" +
            "[{entry value=\"1620684000000\"}]\n" +
            "---\n" +
            "[{label value=\"Responsibility\"}]\n" +
            "[{field type=\"text\" name=\"responsibility\"}]\n" +
            "[{entry value=\"Henno van der Merwe\"}]\n" +
            "---\n" +
            "[{group value=\"Any fertilizer applications done prior to sampling\" display=\"false\"}]\n" +
            "---\n" +
            "[{label value=\"Compliant\"}]\n" +
            "[{field type=\"checkbox\" name=\"compliant1\"}]\n" +
            "[{entry value=\"true\"}]\n" +
            "---\n" +
            "[{label value=\"Evidence/Comment sec supplier\"}]\n" +
            "[{field type=\"textarea\" name=\"evidence/comment_sec_supplier1\"}]\n" +
            "---\n" +
            "[{label value=\"Timeline\"}]\n" +
            "[{field type=\"datetime\" name=\"timeline1\"}]\n" +
            "---\n" +
            "[{label value=\"Responsibility\"}]\n" +
            "[{field type=\"text\" name=\"responsibility1\"}]\n" +
            "---\n" +
            "[{group value=\"Depth of sample\" display=\"false\"}]\n" +
            "---\n" +
            "[{label value=\"Compliant\"}]\n" +
            "[{field type=\"checkbox\" name=\"compliant11\"}]\n" +
            "[{entry value=\"false\"}]\n" +
            "---\n" +
            "[{label value=\"Evidence/Comment sec supplier\"}]\n" +
            "[{field type=\"textarea\" name=\"evidence/comment_sec_supplier11\"}]\n" +
            "---\n" +
            "[{label value=\"Timeline\"}]\n" +
            "[{field type=\"datetime\" name=\"timeline11\"}]\n" +
            "---\n" +
            "[{label value=\"Responsibility\"}]\n" +
            "[{field type=\"text\" name=\"responsibility11\"}]\n" +
            "---\n" +
            "[{group value=\"Position of representative block to be indicated on farm map\" display=\"false\"}]\n" +
            "---\n" +
            "[{label value=\"Compliant\"}]\n" +
            "[{field type=\"checkbox\" name=\"compliant12\"}]\n" +
            "[{entry value=\"false\"}]\n" +
            "---\n" +
            "[{label value=\"Evidence/Comment sec supplier\"}]\n" +
            "[{field type=\"textarea\" name=\"evidence/comment_sec_supplier12\"}]\n" +
            "---\n" +
            "[{label value=\"Timeline\"}]\n" +
            "[{field type=\"datetime\" name=\"timeline12\"}]\n" +
            "---\n" +
            "[{label value=\"Responsibility\"}]\n" +
            "[{field type=\"text\" name=\"responsibility12\"}]\n" +
            "---\n" +
            "[{group value=\"Results of recent plant analyses results (if applicable)\" display=\"false\"}]\n" +
            "---\n" +
            "[{label value=\"Compliant\"}]\n" +
            "[{field type=\"checkbox\" name=\"compliant111\"}]\n" +
            "[{entry value=\"false\"}]\n" +
            "---\n" +
            "[{label value=\"Evidence/Comment sec supplier\"}]\n" +
            "[{field type=\"textarea\" name=\"evidence/comment_sec_supplier111\"}]\n" +
            "---\n" +
            "[{label value=\"Timeline\"}]\n" +
            "[{field type=\"datetime\" name=\"timeline111\"}]\n" +
            "---\n" +
            "[{label value=\"Responsibility\"}]\n" +
            "[{field type=\"text\" name=\"responsibility111\"}]\n" +
            "---\n" +
            "[{group value=\"Records of fertiliser recommendations, and what it is based on. \" display=\"false\"}]\n" +
            "[{description value=\"Elaborate on how strictly the program is followed\"}]\n" +
            "---\n" +
            "[{label value=\"Compliant\"}]\n" +
            "[{field type=\"checkbox\" name=\"compliant2\"}]\n" +
            "[{entry value=\"false\"}]\n" +
            "---\n" +
            "[{label value=\"Evidence/Comment sec supplier\"}]\n" +
            "[{field type=\"textarea\" name=\"evidence/comment_sec_supplier2\"}]\n" +
            "---\n" +
            "[{label value=\"Timeline\"}]\n" +
            "[{field type=\"datetime\" name=\"timeline2\"}]\n" +
            "---\n" +
            "[{label value=\"Responsibility\"}]\n" +
            "[{field type=\"text\" name=\"responsibility2\"}]\n" +
            "---\n" +
            "[{group value=\"Records of fertiliser applications\" display=\"false\"}]\n" +
            "---\n" +
            "[{label value=\"Compliant\"}]\n" +
            "[{field type=\"checkbox\" name=\"compliant3\"}]\n" +
            "[{entry value=\"false\"}]\n" +
            "---\n" +
            "[{label value=\"Evidence/Comment sec supplier\"}]\n" +
            "[{field type=\"textarea\" name=\"evidence/comment_sec_supplier3\"}]\n" +
            "---\n" +
            "[{label value=\"Timeline\"}]\n" +
            "[{field type=\"datetime\" name=\"timeline3\"}]\n" +
            "---\n" +
            "[{label value=\"Responsibility\"}]\n" +
            "[{field type=\"text\" name=\"responsibility3\"}]\n" +
            "---\n" +
            "[{group value=\"Indication of organic material/organic products applied to soil, including manure or compost \" display=\"false\"}]\n" +
            "[{description value=\"amount applied, source material and analyses/chemical composition\"}]\n" +
            "---\n" +
            "[{label value=\"Compliant\"}]\n" +
            "[{field type=\"checkbox\" name=\"compliant4\"}]\n" +
            "[{entry value=\"false\"}]\n" +
            "---\n" +
            "[{label value=\"Evidence/Comment sec supplier\"}]\n" +
            "[{field type=\"textarea\" name=\"evidence/comment_sec_supplier4\"}]\n" +
            "---\n" +
            "[{label value=\"Timeline\"}]\n" +
            "[{field type=\"datetime\" name=\"timeline4\"}]\n" +
            "---\n" +
            "[{label value=\"Responsibility\"}]\n" +
            "[{field type=\"text\" name=\"responsibility4\"}]\n" +
            "---\n" +
            "[{group value=\"Soil carbon content monitoring: Recent analyses of the soil organic carbon/material\" display=\"false\"}]\n" +
            "---\n" +
            "[{label value=\"Compliant\"}]\n" +
            "[{field type=\"checkbox\" name=\"compliant41\"}]\n" +
            "[{entry value=\"false\"}]\n" +
            "---\n" +
            "[{label value=\"Evidence/Comment sec supplier\"}]\n" +
            "[{field type=\"textarea\" name=\"evidence/comment_sec_supplier41\"}]\n" +
            "---\n" +
            "[{label value=\"Timeline\"}]\n" +
            "[{field type=\"datetime\" name=\"timeline41\"}]\n" +
            "---\n" +
            "[{label value=\"Responsibility\"}]\n" +
            "[{field type=\"text\" name=\"responsibility41\"}]\n" +
            "---\n" +
            "[{group value=\"Indication of soil cover management/soil C management strategy\" display=\"false\"}]\n" +
            "[{description value=\"are cover crops used, mulching, crop rotation etc.\"}]\n" +
            "---\n" +
            "[{label value=\"Compliant\"}]\n" +
            "[{field type=\"checkbox\" name=\"compliant42\"}]\n" +
            "[{entry value=\"false\"}]\n" +
            "---\n" +
            "[{label value=\"Evidence/Comment sec supplier\"}]\n" +
            "[{field type=\"textarea\" name=\"evidence/comment_sec_supplier42\"}]\n" +
            "---\n" +
            "[{label value=\"Timeline\"}]\n" +
            "[{field type=\"datetime\" name=\"timeline42\"}]\n" +
            "---\n" +
            "[{label value=\"Responsibility\"}]\n" +
            "[{field type=\"text\" name=\"responsibility42\"}]\n" +
            "---\n" +
            "[{group value=\"Soil microbial management: Analyses done to monitor soil health (soil microbial diversity).\" display=\"false\"}]\n" +
            "[{description value=\"is cover crops used, mulching, crop rotation etc.\"}]\n" +
            "---\n" +
            "[{label value=\"Compliant\"}]\n" +
            "[{field type=\"checkbox\" name=\"compliant43\"}]\n" +
            "[{entry value=\"false\"}]\n" +
            "---\n" +
            "[{label value=\"Evidence/Comment sec supplier\"}]\n" +
            "[{field type=\"textarea\" name=\"evidence/comment_sec_supplier43\"}]\n" +
            "---\n" +
            "[{label value=\"Timeline\"}]\n" +
            "[{field type=\"datetime\" name=\"timeline43\"}]\n" +
            "---\n" +
            "[{label value=\"Responsibility\"}]\n" +
            "[{field type=\"text\" name=\"responsibility43\"}]\n" +
            "+++";

}
