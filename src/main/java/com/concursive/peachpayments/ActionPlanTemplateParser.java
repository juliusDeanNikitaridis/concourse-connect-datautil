package com.concursive.peachpayments;

import java.text.Normalizer;
import java.util.ArrayList;

public class ActionPlanTemplateParser {


    public static void main (String args[]) {

        String[] elements = ActionPlanFile.fileString.split("---");
        String firstLine = "label_value,field_type,entry_value\n";

        ArrayList<FormElement> formElementArrayList = new ArrayList<FormElement>();
        for(String element : elements) {
            String[] childElements = element.split("\\[\\{");
            FormElement formElement = new FormElement();
            for(String ele : childElements) {
                if(ele.startsWith("label value")) {
                    System.out.printf(ele.substring(12,ele.lastIndexOf('"')));
                    formElement.setLabelValue(ele.substring(12,ele.indexOf('"')));
                } else if (ele.startsWith("field type")) {
                    formElement.setFieldType(ele.substring(12,ele.indexOf('"')));
                } else if (ele.startsWith("entry value")) {
                    formElement.setEntryValue(ele.substring(12,ele.indexOf('"')));
                } else {
                    System.out.printf("Form Elelent could not be passed"+ele);
                }
            }
            formElementArrayList.add(formElement);
        }

        for(FormElement fe : formElementArrayList) {
            System.out.printf(fe.toString());
            System.out.printf("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        }
    }


    /**
     * [{label value="Compliant"}]
     * [{field type="checkbox" name="compliant2"}]
     * [{entry value="false"}]
     */
    public static class FormElement {
        @Override
        public String toString() {
            return "FormElement{" +
                    "labelValue='" + labelValue + '\'' +
                    ", fieldType='" + fieldType + '\'' +
                    ", entryValue='" + entryValue + '\'' +
                    '}';
        }

        String labelValue = null;
        String fieldType = null;
        String entryValue = null;

        public FormElement(String labelValue, String fieldType, String entryValue) {
            this.labelValue = labelValue;
            this.fieldType = fieldType;
            this.entryValue = entryValue;
        }

        public FormElement() {

        }


        public String getLabelValue() {
            return labelValue;
        }

        public void setLabelValue(String labelValue) {
            this.labelValue = labelValue;
        }

        public String getFieldType() {
            return fieldType;
        }

        public void setFieldType(String fieldType) {
            this.fieldType = fieldType;
        }

        public String getEntryValue() {
            return entryValue;
        }

        public void setEntryValue(String entryValue) {
            this.entryValue = entryValue;
        }

    }

}
