//package com.concursive.peachpayments;
//
//import javax.net.ssl.HttpsURLConnection;
//import java.io.DataOutputStream;
//import java.io.InputStream;
//import java.net.ProtocolException;
//import java.net.URL;
//import java.sql.SQLException;
//
//import org.apache.commons.io.IOUtils;
//import org.json.JSONObject;
//
//import javax.net.ssl.HttpsURLConnection;
//import java.io.DataOutputStream;
//import java.io.InputStream;
//
//
//public class APITestClient {
//    //    Authorization: Bearer OGFjN2E0Yzk3NTljY2NmYjAxNzViMjUwMGQ5ODJiNzF8VGRKUzV0QTNyWA==
//    //    3DSecure Channel/Entity ID:  8ac7a4ca759cd7850175b2502f7c3202
//    //    Recur Channel/Entity ID: 8ac7a4ca759cd7850175b2502f7c3202
//
//
//    final String AMOUNT = "20.00";
//    final String AUTH_TOKEN = "Bearer OGFjN2E0Yzk3NTljY2NmYjAxNzViMjUwMGQ5ODJiNzF8VGRKUzV0QTNyWA==";
//    final String entityId = "8ac7a4ca759cd7850175b2502f7c3202";
//    final String setupTransactionApiUrl = "https://sandboxconsole.peachpayments.com/v1/checkouts";
//    // final String setupTransactionApiUrl ="https://test.oppwa.com/v1/checkouts";
//
//
//
//    public static void main(String[] args) throws Exception {
//        APITestClient instance = new APITestClient();
//        //instance.setupTransaction();
//        instance.checkTransactionStatus();
//    }
//
//
//    public void setupTransaction() throws Exception {
//
//        URL url = new URL(setupTransactionApiUrl);
//        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
//        conn.setRequestMethod("POST");
//        conn.setRequestProperty("Authorization", AUTH_TOKEN);
//        conn.setDoInput(true);
//        conn.setDoOutput(true);
//
//        String data = ""
//                + "entityId=" + entityId
//                + "&amount=" + AMOUNT
//                + "&currency=ZAR"
//                + "&paymentType=DB";
//
//        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
//        wr.writeBytes(data);
//        wr.flush();
//        wr.close();
//        int responseCode = conn.getResponseCode();
//        InputStream is;
//
//        if (responseCode >= 400) {
//            is = conn.getErrorStream();
//        } else {
//            is = conn.getInputStream();
//        }
//        // actionContext.setAttribute("peach_transaction_id",IOUtils.toString(is));
//        String temp = IOUtils.toString(is);
//        temp = temp.replaceAll("\\{", "[");
//        temp = temp.replaceAll("\\}", "]");
//        System.out.printf(temp);
//    }
//
//
//    public void checkTransactionStatus() throws Exception {
//        //URL checkTransactionURL = new URL("https://test.oppwa.com/v1/checkouts/" + peachTransactionId + "/payment      " + "&entityId=" + entityId);
//        URL checkTransactionUrl = new URL("https://test.oppwa.com/v1/checkouts/169B0D3CB089177346036E175AFC96EC.uat01-vm-tx01/payment&entityId=8ac7a4ca759cd7850175b2502f7c3202");
//        InputStream is = null;
//        try {
//
//            HttpsURLConnection conn = (HttpsURLConnection) checkTransactionUrl.openConnection();
//            conn.setRequestMethod("GET");
//            conn.setRequestProperty("Authorization", AUTH_TOKEN);
//            int responseCode = conn.getResponseCode();
//
//            if (responseCode >= 400) is = conn.getErrorStream();
//            else is = conn.getInputStream();
//            System.out.println(IOUtils.toString(is));
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}
